import { useState } from "react";
import Data from "./data.json";
import Metadata from "./metadata.json";
import Categories from "./categorized.json";
import All from "./all.json";
import Users from "./users.json";
import {
  Badge,
  Box,
  Button,
  Center,
  Container,
  Heading,
  HStack,
  Input,
  InputGroup,
  InputRightElement,
  Select,
  Stack,
  Text,
  VStack,
} from "@chakra-ui/react";
import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  BarChart,
  Bar,
  Legend,
} from "recharts";

const notif = (info, type) => alert(`${type} - ${info}`);

function App() {
  const [user, setUser] = useState(null);
  const isLogged = user !== null;

  return (
    <main>
      <AppBar isLogged={isLogged} user={user} setUser={setUser} />
      <Container maxW="container.xl" mb={10}>
        {isLogged ? <Main /> : <Login user={user} setUser={setUser} />}
      </Container>
    </main>
  );
}

function AppBar({ isLogged, user, setUser }) {
  return (
    <Center bg="blue.200" height={16} mb={4} gap="10rem">
      <Heading size="md">Prototype LaSTI Kelompok 9</Heading>
      <HStack gap={2}>
        {isLogged ? <Text>Selamat datang <em>{user.name}</em></Text> : null}
        {isLogged ? (
          <Button
            colorScheme="red"
            onClick={() => setUser(null)}
            variant="solid"
          >
            Log Out
          </Button>
        ) : null}
      </HStack>
    </Center>
  );
}

function Login({ setUser }) {
  const [show, setShow] = useState(false);
  const toggleShow = () => setShow(!show);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const onSubmit = (e) => {
    e.preventDefault();
    const found = Users.find((v) => v.username === username);
    const match = found ? found.password === password : false;
    if (!match) return notif("Invalid username / password", "error");
    setUser({
      username: username,
      name: found.name,
    });
  };

  return (
    <Center>
      <form onSubmit={onSubmit}>
        <Heading mb={4} size="md">
          Login
        </Heading>
        <Stack spacing={4} width="sm">
          <Input
            placeholder="Username"
            type="text"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
          <InputGroup size="md">
            <Input
              pr="4.5rem"
              type={show ? "text" : "password"}
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <InputRightElement width="4.5rem">
              <Button h="1.75rem" size="sm" onClick={toggleShow}>
                {show ? "Hide" : "Show"}
              </Button>
            </InputRightElement>
          </InputGroup>
          <Button type="submit">Submit</Button>
        </Stack>
      </form>
    </Center>
  );
}

function Main() {
  return (
    <VStack gap={10}>
      <Summary />
      <Categorized />
      <Each />
    </VStack>
  );
}

function Summary() {
  return (
    <VStack gap={2}>
      <Heading size="lg">Summary</Heading>
      <LineChart width={800} height={300} data={All}>
        <Line type="monotone" dataKey="action" stroke="#2ecc71" />
        <Line type="monotone" dataKey="visit" stroke="#3498db" />
        <Line type="monotone" dataKey="share" stroke="#e67e22" />
        <CartesianGrid stroke="#ccc" strokeDasharray="3 3" />
        <XAxis dataKey="month" />
        <YAxis />
        <Tooltip cursor={false} />
        <Legend />
      </LineChart>
    </VStack>
  );
}

function Categorized() {
  return (
    <VStack gap={2}>
      <Heading size="lg">Categorized</Heading>
      <BarChart width={800} height={300} data={Categories}>
        <Bar dataKey="visit" fill="#34495e" />
        <Bar dataKey="action" fill="#9b59b6" />
        <Bar dataKey="share" fill="#f1c40f" />
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip cursor={false} />
        <Legend />
      </BarChart>
    </VStack>
  );
}

const Months = [
  "07/22",
  "08/22",
  "09/22",
  "10/22",
  "11/22 Predict",
  "12/22 Predict"
]

const transform = data => {
  const {visit, action, share} = data
  const res = []
  const r = [0,1,2,3,4,5]
  r.forEach(x => {
    res.push({
      name: Months[x],
      visit: visit[x],
      action: action[x],
      share: share[x],
    })
  })
  return {
    data: res,
    author: data.author,
    category: data.category
  }
}

function Each() {
  const [name, setName] = useState("Braga");
  const [data, setData] = useState(transform(Data.find((d) => d.name === name)));
  const onChange = (e) => {
    setData(
      transform(
        Data.find(d => d.name === e.target.value)
      )
    )
    setName(e.target.value)
  }

  return (
    <VStack gap={2}>
      <Heading size="lg">Search by API Name</Heading>

      <Box>
        <label htmlFor="name">Name</label>
        <Select
          id="name"
          value={name}
          onChange={onChange}
        >
          {Metadata.names.map((n) => (
            <option value={n} key={n}>
              {n}
            </option>
          ))}
        </Select>
        
      </Box>
        <Text>Author : <Badge colorScheme="purple">{data.author}</Badge></Text>
        <Text>Category : <Badge colorScheme="blue">{data.category}</Badge></Text>
      <BarChart width={800} height={300} data={data.data}>
        <Bar dataKey="visit" stackId="a" fill="#16a085" />
        <Bar dataKey="action" stackId="a" fill="#273c75" />
        <Bar dataKey="share" stackId="a" fill="#c0392b" />
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip cursor={false} />
        <Legend />
      </BarChart>
    </VStack>
  );
}

export default App;
